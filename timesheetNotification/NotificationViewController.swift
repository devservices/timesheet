//
//  NotificationViewController.swift
//  timesheetNotification
//
//  Created by Roodyn, Yishai (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet weak var firstClientView: UIView!
    @IBOutlet var clientViews: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        let jakesLightGreyColor: UIColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.00)
        let thickness: CGFloat = 1
        
        for borderView in self.clientViews {
            let topBorder = CALayer()
            topBorder.frame = CGRect.init(x: 0, y: 0, width: borderView.frame.width, height: thickness)
            topBorder.backgroundColor = jakesLightGreyColor.cgColor
            borderView.layer.insertSublayer(topBorder, at: 0)
        }
        
        let topBorder = CALayer()
        topBorder.frame = CGRect.init(x: 0, y: 0, width: self.firstClientView.frame.width, height: thickness)
        topBorder.backgroundColor = jakesLightGreyColor.cgColor
        self.firstClientView.layer.insertSublayer(topBorder, at: 0)

    }
    
}
