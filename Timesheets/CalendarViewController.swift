//
//  ViewController.swift
//  Timesheets
//
//  Created by Roodyn, Yishai (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI


class CalendarViewController: UIViewController  {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    let days:[Day] = [.monday, .tuesday, .wednesday, .thursday, .friday]
    
    enum Segues:String {
        case settings = "SettingsSegue"
    }
    
    enum ReuseIdentifiers:String {
        case calendarHeader = "CalendarHeader"
        case calendarCell = "CalendarTableViewCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavBar()
        configureSegmentedControl()
        configureTableView()
        view.backgroundColor = .backgroundGrey()
    }
    
    private func configureTableView() {
        tableView.backgroundColor = .backgroundGrey()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName:"CalendarTableViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: ReuseIdentifiers.calendarHeader.rawValue)
        tableView.register(UINib(nibName:"CalendarTableViewCell", bundle:nil), forCellReuseIdentifier: ReuseIdentifiers.calendarCell.rawValue)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    private func configureNavBar() {
        title = "Timesheet"
        
        let item = UIBarButtonItem(image: UIImage(named:"settings-button"), style: .plain, target: self, action:#selector(CalendarViewController.openSettings))
        
        navigationItem.rightBarButtonItem = item
        
        navigationController?.navigationBar.shadowImage = UIImage();
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        navigationController?.navigationBar.tintColor = UIColor(red:0.44, green:0.87, blue:0.29, alpha:1.00)
        
        navigationController?.navigationBar.backgroundColor = .backgroundGrey()
        
    }
    
    private func configureSegmentedControl() {
        segmentedControl.selectedSegmentIndex = 1
    }
    
    func openSettings() {
        performSegue(withIdentifier:Segues.settings.rawValue, sender: self)
    }
    
}

extension CalendarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.calendarCell.rawValue) as! CalendarTableViewCell
        
        let startOfWeek = Date().startOfWeek
        
        if indexPath.section == 0 {
            cell.updateCell(day: days[indexPath.row], startOfWeek: startOfWeek!)
        } else {
            let week2 = Calendar.current.date(byAdding: .day, value: 7, to: startOfWeek!)
            cell.updateCell(day: days[indexPath.row], startOfWeek: week2!)
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return tableView.dequeueReusableHeaderFooterView(withIdentifier: ReuseIdentifiers.calendarHeader.rawValue)
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 50
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}



