//
//  PreferencesViewController.swift
//  Timesheets
//
//  Created by Mitchinson, Jacob (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let settingOptions: [String] = ["Staffit", "Location", "SAP History", "Calendar", "Email"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.navigationItem.title = "Settings"
        
        self.view.backgroundColor = .backgroundGrey()
        
        // Do any additional setup after loading the view.
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCellIdentifier", for: indexPath) as! SettingsTableViewCell
        cell.label?.text = settingOptions[indexPath.row]
        if let image = UIImage(named: settingOptions[indexPath.row]) {
            cell.iconView?.image = image
        }
        return cell
    }
    
}
