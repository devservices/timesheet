//
//  CalendarData.swift
//  Timesheets
//
//  Created by Mitchinson, Jacob (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit

enum Client:String {
    
    case apple = "Apple Inc"
    case bank = "HSBC Holdings PLC"
    case grocery = "J Sainsbury PLC"
    case hospital = "Great Ormond Street Hospital C"
    case crops = "Syngenta Crop Protection AG"
    
    var color:UIColor {
        switch self {
        case .apple:
            return .brightGreen()
        case .bank:
            return .brightBlue()
        default:
            return .brightGreen()
        }
    }
    
}

enum Day:String {
    
    case monday = "Mon"
    case tuesday = "Tue"
    case wednesday = "Wed"
    case thursday = "Thu"
    case friday = "Fri"
    
    var client:Client {
        switch self {
        case .monday:
            return Client.apple
        case .tuesday:
            return Client.bank
        case .wednesday:
            return Client.apple
        case .thursday:
            return Client.apple
        case .friday:
            return Client.bank
        }
    }
    
}

extension Date {
    struct Gregorian {
        static let calendar = Calendar(identifier: .gregorian)
    }
    var startOfWeek: Date? {
        return Gregorian.calendar.date(from: Gregorian.calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
}
