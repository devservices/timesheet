//
//  Colors.swift
//  Timesheets
//
//  Created by Mitchinson, Jacob (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func backgroundGrey() -> UIColor {
        return UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.00)
    }
    
    static func darkGreyTextColor() -> UIColor {
        return UIColor(red:0.38, green:0.38, blue:0.38, alpha:1.00)
    }
    
    static func lightGreyTextColor() -> UIColor {
        return UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.00)
    }
    
    static func brightGreen() -> UIColor {
        return UIColor(red:0.44, green:0.87, blue:0.29, alpha:1.00)
    }
    
    static func brightBlue() -> UIColor {
        return UIColor(red:0.07, green:0.85, blue:0.90, alpha:1.00)
    }
    
}
