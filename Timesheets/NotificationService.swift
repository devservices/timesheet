//
//  NotificationService.swift
//  Timesheets
//
//  Created by Mitchinson, Jacob (UK - London) on 22/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit
import UserNotifications

struct NotificationService {
    
    static func scheduleNotification() {
//        let calendar = Calendar(identifier: .gregorian)
//        let components = calendar.dateComponents(in: .current, from: date)
//        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        //        let trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "Time Sheet Submission"
        content.body = "Hold down to view and submit your time sheet"
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "localTimeSheetReminder"
        
        let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().setNotificationCategories([self.addActions()])
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
    }
    
    private static func addActions() -> UNNotificationCategory {
        let submitAction = UNNotificationAction(identifier: "submit", title: "Submit", options:UNNotificationActionOptions.authenticationRequired)
        let viewAction = UNNotificationAction(identifier: "view", title: "Change", options:UNNotificationActionOptions.foreground)
        return UNNotificationCategory(identifier: "localTimeSheetReminder", actions: [submitAction, viewAction], intentIdentifiers: [], options: [])
    }
    
}
