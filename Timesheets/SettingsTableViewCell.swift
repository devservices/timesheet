//
//  SettingsTableViewCell.swift
//  Timesheets
//
//  Created by Roodyn, Yishai (UK - London) on 22/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
