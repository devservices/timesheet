//
//  LoginViewController.swift
//  Timesheets
//
//  Created by Dubey, Josh (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit

class LoginViewController : UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var tokenTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    
}


extension LoginViewController : UITextFieldDelegate {
    
}
