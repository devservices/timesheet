//
//  CalendarTableViewHeader.swift
//  Timesheets
//
//  Created by Mitchinson, Jacob (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit

class CalendarTableViewHeader: UITableViewHeaderFooterView {
   
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .backgroundGrey()
        dateLabel.textColor = .darkGreyTextColor()
    }
    
    @IBAction func queueNotification(_ sender: AnyObject) {
        
        print("Notification Queued")
        
        NotificationService.scheduleNotification()
        
    }
}
