//
//  CalendarTableViewCell.swift
//  Timesheets
//
//  Created by Mitchinson, Jacob (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit

class CalendarTableViewCell: UITableViewCell {

    @IBOutlet var lines: [UIView]!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var clientLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var colorViewBottomContstraint: NSLayoutConstraint!

    let days:[Day] = [.monday, .tuesday, .wednesday, .thursday, .friday]
    
    override func prepareForReuse() {
        super.prepareForReuse()
        colorViewBottomContstraint.constant = 4
        clientLabel.isHidden = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .backgroundGrey()
        
        selectionStyle = .none
        
        dateLabel.textColor = .lightGreyTextColor()
        
        for lineView in lines {
            lineView.backgroundColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.00)
        }
        
        colorView.backgroundColor = .brightGreen()
        
    }
    
    func updateCell(day:Day, startOfWeek:Date) {
        clientLabel.text = day.client.rawValue
        colorView.backgroundColor = day.client.color
        
        calculateColorPosition(day: day)
    
        let date = Calendar.current.date(byAdding: .day, value: day.hashValue, to: startOfWeek)
        let calendar = Calendar(identifier: .gregorian)
        let dateComponents = calendar.dateComponents([.day], from: date!)
        
        dateLabel.text = "\(dateComponents.day! + 1) \(day.rawValue)"
    }
    
    func calculateColorPosition(day:Day) {
        var nextDayIndex = days.index(of: day)! + 1
        var previousDayIndex = days.index(of: day)! - 1
        
        if day == .friday {
            // next day is monday
            nextDayIndex = 0
        }
        
        if day == .monday {
            // previous day is friday
            previousDayIndex = 4
        }
        
        let nextDay = days[nextDayIndex]
        let previousDay = days[previousDayIndex]
        
        if nextDay.client == day.client {
            colorViewBottomContstraint.constant = 0
        }
        
        if previousDay.client == day.client {
            clientLabel.isHidden = true
            colorView.layer.zPosition = 1
        }
    }
}
