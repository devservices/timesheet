//
//  ViewController.swift
//  Timesheets
//
//  Created by Roodyn, Yishai (UK - London) on 21/11/2016.
//  Copyright © 2016 DeloitteDigital. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import UserNotifications


class ViewController: UIViewController, UNUserNotificationCenterDelegate  {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().delegate = self;
        configureNavBar()
        configureSegmentedControl()
    }
    
    private func configureNavBar() {
        title = "Timesheet"
        
        let item = UIBarButtonItem(image: UIImage(named:"settings-button"), style: .plain, target: self, action:#selector(ViewController.openSettings))
        
        navigationItem.rightBarButtonItem = item
        
        navigationController?.navigationBar.shadowImage = UIImage();
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        navigationController?.navigationBar.tintColor = UIColor(red:0.44, green:0.87, blue:0.29, alpha:1.00)
    }
    
    private func configureSegmentedControl() {
        segmentedControl.selectedSegmentIndex = 1
    }
    
    func openSettings() {
        performSegue(withIdentifier:"preferencesSegue", sender: self)
    }
    
    @IBAction func datePickerDidSelectNewDate(_ sender: UIDatePicker) {
        let selectedDate = sender.date
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.scheduleNotification(at: selectedDate)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
        if response.actionIdentifier == "submit" {
            
            //TODO: Submit Time Sheet here!!
            print("Submitting Time Sheet")
        }
    }
    
}


